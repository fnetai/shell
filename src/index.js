import shell from 'shelljs';

export default (args) => {
  const defaultOptions = { silent: false };
  const isStringArgs = typeof args === 'string';
  const command = isStringArgs ? args : args.cmd || `echo "No command provided. Args received: ${JSON.stringify(args)}"`;
  const options = isStringArgs ? defaultOptions : { ...defaultOptions, ...args.options };
  const wait = args.wait !== false;

  if (options.async && !wait) {
    shell.exec(command, options);
    return Promise.resolve({ code: 0, stdout: 'Command is running in the background.', stderr: '', args });
  }

  return new Promise((resolve, reject) => {
    shell.exec(command, options, (code, stdout, stderr) => {
      const result = { code, stdout, stderr, args };
      code === 0 ? resolve(result) : reject(result);
    });
  });
};