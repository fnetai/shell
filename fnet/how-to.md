# Developer Guide for @fnet/shell

## Overview

The `@fnet/shell` library simplifies the execution of shell commands from within a Node.js application. The primary functionality of this library is to execute shell commands asynchronously or synchronously, handling standard output and errors with ease. This allows developers to seamlessly integrate shell command execution into their applications without dealing with low-level process management.

## Installation

To install the `@fnet/shell` package, you can use either npm or yarn. Here are the commands:

Using npm:
```bash
npm install @fnet/shell
```

Using yarn:
```bash
yarn add @fnet/shell
```

## Usage

The library provides a single exported function which allows you to execute shell commands with specified options.

### Basic Usage

To execute a shell command, you can directly pass it as a string. This will execute the command and return a promise that resolves with the result of the command execution.

```javascript
import shell from '@fnet/shell';

shell('ls -l')
  .then(result => {
    console.log('Command executed successfully:');
    console.log(result.stdout);
  })
  .catch(error => {
    console.error('Command execution failed:', error);
  });
```

### Advanced Usage

If you need more control over the execution, such as specifying options or handling command execution asynchronously, you can pass an object with a `cmd` property and options.

```javascript
import shell from '@fnet/shell';

// Execute a command with specific options
shell({
  cmd: 'echo "Hello, World!"',
  options: { silent: true }
})
  .then(result => {
    console.log('Command Output:', result.stdout);
  })
  .catch(error => {
    console.error('Error:', error.stderr);
  });

// Running a command asynchronously
shell({
  cmd: 'long-running-command',
  options: { async: true }
})
  .then(result => {
    console.log(result.stdout); // "Command is running in the background."
  });
```

## Examples

Here are some examples to demonstrate typical use cases of the library:

### Example 1: Synchronous Command Execution

Execute a simple shell command to list files in a directory:
```javascript
shell('ls').then(result => {
  console.log(result.stdout);
});
```

### Example 2: Asynchronous Command Execution

Run a shell command in the background:
```javascript
shell({ cmd: 'sleep 10', options: { async: true } })
  .then(result => {
    console.log(result.stdout); // Outputs: "Command is running in the background."
  });
```

### Example 3: Error Handling

Handle errors if the shell command fails:
```javascript
shell('invalid-command')
  .catch(error => {
    console.error(`Error executing command: ${error.stderr}`);
  });
```

## Acknowledgement

`@fnet/shell` leverages the `shelljs` library to execute shell commands, which provides a set of portable Unix shell commands within Node.js environments.

This guide delivers a practical understanding of the `@fnet/shell` library, allowing developers to effectively execute shell commands within their Node.js applications.